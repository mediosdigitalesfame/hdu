<!-- INICIO NUESTROS VEHICULOS -->
	<div id="content">

			<div class="latest-post">
				<div class="title-section">
					<h1> Honda Civic 2018 </h1>
				</div>
                
				<div id="owl-demo" class="owl-carousel owl-theme">

					  

 					 <div class="item news-item">
	                   	 <center>
							 <a href="pdfs/civic2018.pdf" target="_blank"><img alt="Civic Coupe 2018" src="images/autos/civic2018.png"></a>
							  <h2> <strong> <em> CIVIC <sup>&reg;</sup> <font color="#DD1707">2018</font> </em> </strong></h2> </font>  
							 <a class="read-more" href="pdfs/civic2018.pdf" target="_blank">Características <i class="fa fa-arrow-right"></i></a>
	                     </center>
 					 </div>

 					 <div class="item news-item">
	                   	 <center>
							 <a href="pdfs/civiccoupe2018.pdf" target="_blank"><img alt="Civic Coupe 2018" src="images/autos/civiccoupe2018.png"></a>
							  <h2> <strong> <em> CIVIC COUPE <sup>&reg;</sup> <font color="#DD1707">2018</font> </em> </strong></h2> </font>  
							 <a class="read-more" href="pdfs/civiccoupe2018.pdf" target="_blank">Características <i class="fa fa-arrow-right"></i></a>
	                     </center>
 					 </div>

 					 
</div>
			</div>
<!-- FIN NUESTROS AUTOS -->