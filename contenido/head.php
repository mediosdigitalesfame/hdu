 
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="Title" content="FAME Honda Uruapan">
	<meta name="description" content="Conéctate con el espíritu de la innovación automotriz e impulsa el poder de tus sueños en Honda Uruapan. Piensa en auto, piensa en FAME.">
    <meta name="keywords" content="Nuevo Fit 2016, Civic Turbo, Honda Michoacán, Honda Uruapan, Honda uruapan, honda fame, fame, Grupo Fame, Honda michoacan, grupo fame uruapan, agencia honda uruapan, fame uruapan, Uruapan, México, Autos uruapan, Nuevos honda, Seminuevos honda, seminuevos uruapan, seminuevos michoacan, Agencia uruapan, Servicio uruapan, Taller uruapan, Hojalatería uruapan, hojalateria, Pintura uruapan, postventa, accord, accord coupe, accord sedan, city, honda city, civic, civic coupe, civic sedan, civic si, civic hybrid, crosstour, cr-v, crv, cr-z, crz, fit, honda fit, odyssey, pilot, honda pilot, ridgeline, honda 2015, Lujo, honda 2014, The power of Dreams, ASIMO, Premium, 2014">
    <meta name="author" content="Grupo FAME División Automotriz">
    
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,700,600,300' rel='stylesheet' type='text/css'>

	<link rel="stylesheet" type="text/css" href="css/bootstrap.css"  media="screen">
	<link rel="stylesheet" type="text/css" href="css/fullwidth.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="css/settings.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="css/magnific-popup.css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/owl.carousel.css" media="screen">
    <link rel="stylesheet" type="text/css" href="css/owl.theme.css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/jquery.bxslider.css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/font-awesome.css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/style.css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/flexslider.css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/responsive.css" media="screen">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

    <link rel="icon" type="image/png" href="/images/favicon.png" />
    
    <!-- Inserta esta etiqueta en la sección "head" o justo antes de la etiqueta "body" de cierre. -->
<script src="https://apis.google.com/js/platform.js" async defer>
  {lang: 'es-419'}
</script>

 