<!doctype html>

<html lang="es" xml:lang="es" class="no-js">

<head>

	<title>Contacto</title>

	<?php include('contenido/head.php'); ?>

</head>



<body> 

<?php include('chat.php'); ?>

	<div id="container">

		<?php include('contenido/header.php'); ?>

		<?php include('contenido/analytics.php'); ?>



		<div id="content">

			<div class="page-banner"> <div class="container"> <h2>Contáctanos</h2> </div> </div>



			<div class="contact-box">

				<div class="container">

					<div class="row">

						<div class="col-md-6" align="center">

 


						<div class="container">

							<div class="col-md-12" >

								<?php include('jotformpost.php'); ?>

							</div>

						</div>

					</div>



					<div class="col-md-3">

						<div class="contact-information">

							<h3>Información de Contacto</h3>

							<ul class="contact-information-list">

								<li><span><i class="fa fa-home"></i>Paseo Lázaro Cárdenas #451</span> <span>Col. Morelos. </span> <span>Uruapan, Michoacán.</span></li>

								<li><span><i class="fa fa-phone"></i>(452) 524 7500</span></li>

								<li>

									<i class="fa fa-phone"></i><span>Postventa <strong>Extensión 105 y 112</strong></span><br>                                  

									<i class="fa fa-phone"></i><span>Refacciones <strong>Extensión 104</strong></span><br>

									<i class="fa fa-phone"></i><span>Ventas <strong>Extensión 109</strong></span><br>

									<i class="fa fa-phone"></i><span>Recepción <strong>Directo</strong></span><br>

									<i class="fa fa-phone"></i><span>Contabilidad <strong>Extensión 108</strong></span><br></li>

									<h3>Whatsapp</h3>

									<li>

										<span><i class="fa fa-whatsapp"></i>

											<strong>   

												Ventas   y   Postventa <br> 

												<a href="https://api.whatsapp.com/send?phone=524431810975 &text=Hola,%20Quiero%20más%20información!" title="Cita de Servicio">

													4431810975

												</a>

												|  

												<a href="https://api.whatsapp.com/send?phone=524425923862 &text=Hola,%20Quiero%20más%20información!" title="Cita de Servicio">

													4521058846

												</a>

											</strong>

										</span>

									</li>    

								</ul>

							</div>

						</div>



						<div class="col-md-3">

							<div class="contact-information">

								<h3>Horario de Atención</h3>

								<p>Con gusto esperamos tu llamada en nuestro <strong>Call Center</strong>, para cualquier duda, aclaración o sugerencia que quieras compartirnos en <strong>FAME Honda Uruapan</strong>; te escuchamos y atendemos de manera personalizada. </p>

								<p class="work-time"><span>Lunes - Viernes</span> : 9:00 a.m. - 7:00 p.m.</p>

								<p class="work-time"><span>Sábado</span> : 9:00 a.m. - 2:00 p.m.</p>

							</div>

						</div>







					</div>

				</div>

			</div>



		</div> 



		<br>



		<?php include('contenido/footer.php'); ?>

	</div> 			



</body>

</html>