<!doctype html>
<html lang="es" xml:lang="es" class="no-js">
<head>
	<title>Cita de Servicio</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="description" content="FAME HONDA Uruapan, Michoacán. Sitio WEB oficial FAME Honda Uruapan. Conoce al grupo automotriz más grande de México y Latinoamérica, con más de 1600 unidades en inventario. Piensa en auto, piensa en FAME. ">
    <meta name="keywords" content="Honda uruapan, honda fame, fame, Grupo Fame, Honda michoacan, grupo fame uruapan, agencia honda uruapan, fame uruapan, Uruapan, México, Autos uruapan, Nuevos honda, Seminuevos honda, seminuevos uruapan, seminuevos michoacan, Agencia uruapan, Servicio uruapan, Taller uruapan, Hojalatería uruapan, hojalateria, Pintura uruapan, postventa, accord, accord coupe, accord sedan, city, honda city, civic, civic coupe, civic sedan, civic si, civic hybrid, crosstour, cr-v, crv, cr-z, crz, fit, honda fit, odyssey, pilot, honda pilot, ridgeline, honda 2015, Lujo, honda 2014, The power of Dreams, ASIMO, Premium, 2014">
    <meta name="author" content="Grupo FAME División Automotriz">
    

	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,700,600,300' rel='stylesheet' type='text/css'>

	<link rel="stylesheet" type="text/css" href="css/bootstrap.css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/font-awesome.css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/style.css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/responsive.css" media="screen">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    
    
    <link rel="icon" type="image/png" href="/images/favicon.png" />    

</head>
<body><em></em>

	<?php include('chat.php'); ?>

	<!-- Container -->
	<div id="container">
		<!-- Header
		    ================================================== -->
		<header class="clearfix">
			<!-- Static navbar -->
			<div class="navbar navbar-default navbar-fixed-top">
				<div class="top-line">
					<div class="container">
						<p>
							 
							<span><i class="fa fa-phone"></i>Agencia: (452) 524 7500 </span>
<!--                            <span><i class="fa fa-user"></i><a href="aviso.html">Aviso de privacidad</a></span>-->
						</p>
						<ul class="social-icons">
							<li><a class="facebook" href="https://www.facebook.com/Honda-de-Uruapan-265750983892691/" target="_blank"><i class="fa fa-facebook"></i></a></li>
							<li><a class="twitter" href="https://twitter.com/HondaFame" target="_blank"><i class="fa fa-twitter"></i></a></li>
							<li><a class="youtube" href="https://www.youtube.com/user/GrupoFameAutos" target="_blank"><i class="fa fa-youtube-play"></i></a></li>
                            <li><a class="instagram" href="https://www.instagram.com/grupofame" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i>
                            <li><a class="whatsapp" href="http://hdu.com.mx/contacto.php" target="_self"><i class="fa fa-whatsapp"></i></a></li>
						</ul>
					</div>
				</div>
                
				<div class="container">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="index.html" target="_self"><img alt="Inicio" src="images/logo.png"></a>
					</div>
					<div class="navbar-collapse collapse">
						<ul class="nav navbar-nav navbar-right">

							<li><a href="index.html">Inicio</a></li>
							<!--<li class="drop"><a>Financiamiento</a>
								 </li>  -->
							<li class="drop"><a href="#">Servicio</a>
								<ul class="drop-down">
									<li><a href="servicio.html">Servicio</a></li>
									<li><a href="servicio.php">Cita de Servicio</a></li>                     
									<li><a href="garantia.html">Garantía Extendida</a></li>
									<li><a href="refacciones.php">Refacciones</a></li>
									<li><a href="privilegios.html">Club de Privilegios</a></li>
                                 </ul> </li>                                                             
							<li class="drop"><a href="autos.html">Vehículos</a>
								<ul class="drop-down">
                                
                                	<li><a href="fichas/fit.pdf" target="_blank">Fit</a></li>
                                	<li><a href="fichas/city.pdf" target="_blank">City</a></li>
                                	<li><a href="fichas/civic_sedan.pdf" target="_blank">Civic Sedán</a></li> 
                                    <li><a href="civic-coupe.php" target="_self">Civic Coupé</a></li>
                                	<li><a href="fichas/accord_sedan.pdf" target="_blank">Accord </a></li>
                                	<li><a href="fichas/crv.pdf" target="_blank">CR-V</a></li>
                                    <li><a href="fichas/hrv.pdf" target="_blank">HR-V</a></li>
                                    <li><a href="fichas/pilot.pdf" target="_blank">Pilot</a></li>
                                    <li><a href="fichas/odissey.pdf" target="_blank">Odyssey</a></li>
                                </ul> </li>
                                                      
							<li class="drop"><a class="active" href="promociones.html">Promociones</a>  
                            		<ul class="drop-down">
									<li><a href="promociones.html">Promociones Vigentes</a></li>
                                    <li><a href="finance.html">Honda Finance</a></li>
									<li><a href="planes.html">Planes - Comisiones</a></li>
									<li><a href="autofinanciamiento.php" target="_blank">Autofinanciamiento</a></li>																																				
                                  </ul>
                            
                              </li>                        
							<li><a href="http://www.fameseminuevos.com/" target="_blank">Seminuevos</a></li>
							<li><a href="contacto.php">Contacto</a></li>
                            <li><a href="ubicacion.html">Ubicación</a></li>
						</ul>
					</div>
				</div>
			</div>
		</header>

		<!-- End Header -->
        
<!-- ANALYTICS HONDA MONARCA DF-->

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-47757359-1', 'auto');
  ga('send', 'pageview');

</script>

<!-- FIN ANALYTICS -->      

		<!-- content 
			================================================== -->
		<div id="content">

			<!-- Page Banner -->
			<div class="page-banner">
				<div class="container">
					<h2>Auto Financiamiento</h2>

				</div>
			</div>


			<!-- contact box -->
			<div class="contact-box">
				<div class="container">
					<div class="row">
					<p align="center">	
                    
             <span class="single-project-content">
                    <img alt="Auto Financiamiento" src="images/autofinanciamiento.jpg"></span>
             
             </p><br>
						
                        <div class="col-md-11" align="center">
						  <h3 align="left">¿Qué es el Autofinanciamiento?</h3>
<p align="justify" align="center">Es un sistema que te permite, tanto a personas físicas o morales, programar la compra de un automóvil ultimo modelo de la marca HONDA.</p><br>

						<h3 align="left">¿Cómo funciona?</h3>
<p align="justify">Consiste en la integración de  grupos de personas, que a través de pagar sus mensualidades puntualmente, tienen el derecho de participar en un Sorteo mensual que les permite adquirir desde su primera mensualidad un automóvil nuevo.<br>
 Además tiene la ventaja que puede adquirirse al auto nuevo a través de otros medios de asignación como:<br><br>
 <p align="justify">•	<strong>Subasta</strong> - Consiste en ofrecer un número determinado de mensualidades por el cliente, para tener la posibilidad de que si es el cliente que más mensualidades ofreció obtendrá el derecho de adquirir su automóvil nuevo.<br>
•	<strong>Orden secuencial </strong>- Consiste en que si después de designar el ganador de sorteo y no hay ofrecimiento de mensualidades por parte de algún integrante del grupo y este cuenta con los recursos suficientes para designar otra unidad se utiliza el orden secuencial del sorteo y se le asigna al cliente que este al corriente en sus pagos y que no tenga la unidad<br>

 <p align="justify"> Actualmente<strong> Autofinanciamiento HONDA</strong> cuenta con planes de entrega garantizada a corto y mediano plazo, que no dependen de Sorteo o subasta abierta.</p><br>
 
 <h3 align="left">B2 Programa tu compra</h3>
 
 <p align="justify">Está diseñado para personas que quieren programar la compra de su automóvil <strong>HONDA.</strong><br>
El clásico promocional de 48 mensualidades se entregan los automóviles vía sorteo y por medio de subastas cerradas no garantizadas que te permiten subastar la cantidad que quieras y esperar a ver el resultado de tu grupo y ver si resultaste ganador para así poder llevarte tu automóvil. <br><br>
• El Plan de Autofinanciamiento más económico del mercado.<br>
• Subasta cerrada.<br>
• Tienes la posibilidad de obtener tu HONDA desde la primera mensualidad.<br>
• Sin Cuota de Inscripción, sin intereses, sin anualidades y sin enganche. <br>
• 48 Mensualidades.<br><br>


<h3 align="left">B4 Tu puntualidad te premia</h3>

<p align="justify">Está diseñado para personas que quieren programar la compra de su automóvil HONDA y obtener beneficios por pagos puntuales.<br>
Los beneficios de los que usted puede gozar al suscribirse en B4 son:<br><br>
• Por cada 11 pagos puntuales y consecutivos Autofinanciamiento HONDA bonificara una mensualidad del final del plazo.<br>
• Tiene un excelente costo financiero si pagan puntualmente.</p><br><br>

<h3 align="left">BG Garantizado Adjudíquese cuando quiera (3+9)</h3>

<p align="justify">Está diseñado para personas que quieren programar la compra de su automóvil HONDA en un periodo de tres meses (a partir de que quede integrado a un grupo) o máximo al mes 11 con una mensualidad de subasta. <br><br>

En el Garantizado 3+9 de 48 mensualidades se entregan los automóviles vía sorteo y por medio de subastas garantizadas que te permiten programar la entrega del vehículo a partir del tercer mes ofreciendo una subasta que complete 12 mensualidades. <br><br>
    
    
    <h1> Cotiza tu HONDA </h1><br><br>
        
<!--Formulario Auto financiamiento---------------->
                    
<?php
	if (isset($_POST['boton'])) {
        if($_POST['nombre'] == '') {
        	$errors[1] = '<span class="error">Ingrese su nombre</span>';
        } else if($_POST['email'] == '' or !preg_match("/^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/",$_POST['email'])){
        	$errors[2] = '<span class="error">Ingrese un email correcto</span>';
        } else if($_POST['telefono'] == '') {
        	$errors[3] = '<span class="error">Ingrese un teléfono</span>';
        } else if($_POST['mensaje'] == '') {
        	$errors[4] = '<span class="error">Ingrese un mensaje</span>';
        } else {
        	$dest = "ventas@hdu.com.mx" . ','. "formas@grupofame.com" . ','. "gerencia@hdu.com.mx";
            $nombre = $_POST['nombre'];
            $email = $_POST['email'];
            $telefono = $_POST['telefono'];
			$asunto_cte = "Auto Financiemiento Honda Uruapan";
			$asunto = "Auto Financiemiento Honda Uruapan";
            $cuerpo = $_POST['mensaje'];
			$cuerpo_mensaje .= $nombre . "<br>" . "Mensaje: ". $cuerpo . "<br>" . "Mi correo es: " . $_POST['email'] . "<br>" . "Mi teléfono es: " . $_POST['telefono'];
			$cuerpo_cte = '
			<html>
			<head>
			  <title>Mail from '. $nombre .'</title> 
			</head>
			<body>
			  <table style="width: 500px; font-family: arial; font-size: 14px;" border="1">
				<tr style="height: 32px;">
				  <th align="right" style="width:150px; padding-right:5px;">Nombre:</th>
				  <td align="left" style="padding-left:5px; line-height: 20px;">'. $nombre .'</td>
				</tr>
				<tr style="height: 32px;">
				  <th align="right" style="width:150px; padding-right:5px;">Correo:</th>
				  <td align="left" style="padding-left:5px; line-height: 20px;">'. $email .'</td>
				</tr>
				<tr style="height: 32px;">
				  <th align="right" style="width:150px; padding-right:5px;">Mensaje:</th>
				  <td align="left" style="padding-left:5px; line-height: 20px;">'. $cuerpo .'</td>
				</tr>
			  </table>
			</body>
			</html>
			';
			$headers  = 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
			$headers .= 'From: ' . $email . "\r\n";
            
            if(mail($dest,$asunto,$cuerpo_mensaje,$headers)){
            	$result = '<div class="result_ok">Email enviado correctamente, en breve nos comunicaremos contigo. Gracias por tu preferencia. "Piensa en Auto, Piensa en FAME"</div>';
				mail($email,$asunto_cte,$cuerpo_cte,$headers);
                $_POST['nombre'] = '';
                $_POST['email'] = '';
                $_POST['telefono'] = '';
                $_POST['mensaje'] = '';
            } else {
            	$result = '<div class="result_fail">Hubo un error al enviar el mensaje</div>';
            }
        }
    }
?>
<html>
	<meta charset="utf-8">
    <link rel='stylesheet'| href='css/formularios.css'>
    <script src='http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js'></script>
	<form id="contact-form" class="contact-work-form2" method='post' action=''>
    	<div class="text-input">
        	<div class="float-input">
            <input name='nombre' id="nombre" placeholder="Nombre*" type='text' class='nombre' value='<?php if(isset($_POST['nombre'])){ echo $_POST['nombre']; } ?>'> 
        <?php if(isset($errors)){ echo $errors[1]; } ?><span><i class="fa fa-user"></i></span></div>
        
        <div class="float-input2"><input name='email' placeholder="E-mail*" type='text' class='email' value='<?php if(isset($_POST['email'])){ echo $_POST['email']; } ?>'>
        <?php if(isset($errors)){ echo $errors[2]; } ?><span><i class="fa fa-envelope"></i></span></div>
        </div>
        
        <div class="text-input">
        <div class="float-input">
        <input name='telefono' id="telefono" placeholder="Teléfono*" type='tel' class='telefono' value='<?php if(isset($_POST['telefono'])){ echo $_POST['telefono']; } ?>'>
        <?php if(isset($errors)){ echo $errors[3]; } ?><span><i class="fa fa-phone"></i></span>
        </div>
        </div>
        
        <div class="textarea-input"><textarea name='mensaje' placeholder="Mensaje*" rows='5' class='mensaje'><?php if(isset($_POST['mensaje'])){ echo $_POST['mensaje']; } ?></textarea>
        <?php if(isset($errors)){ echo $errors[4]; } ?><span><i class="fa fa-comment"></i></span>
        </div>
        <div><input name='boton' type='submit' value='Enviar' class='boton'></div>
        <br><br><br>
<?php if(isset($result)) { echo $result; } ?>
    </form>                            
                            
<!--<p align="justify">*Le recordamos que las citas de manejo y servicio están sujetas a horarios disponibles.</p>   -->                         
                            
						</div>

					</div>
				</div>
			</div>

		</div><br><br><br><br><br>



		<!-- End content -->


		<!-- footer -->
		
<footer>
			<div class="footer-line">
				<div class="container">
					<p>
<span><i class="fa fa-phone"></i> 01800 670 8386 | </span> 2016 Honda de Uruapan | <i class="fa fa-user"> </i><a href="aviso.html"><font color="#FFFFFF"><strong>  Aviso de privacidad</strong></font></a></p>
					<a class="go-top" href="#"></a>
				</div>
			</div>

		</footer>
		<!-- End footer -->
        
	</div>
	<!-- End Container -->

	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/jquery.migrate.js"></script>
	<script type="text/javascript" src="js/bootstrap.js"></script>
  	<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
	<script type="text/javascript" src="js/gmap3.min.js"></script>
	<script type="text/javascript" src="js/retina-1.1.0.min.js"></script>
	<script type="text/javascript" src="js/plugins-scroll.js"></script>
	<script type="text/javascript" src="js/script.js"></script>

	<!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	
</body>
</html>